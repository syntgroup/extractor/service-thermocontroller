include(FetchContent)
set(CMAKE_TLS_VERIFY true)


# Avoid warning about DOWNLOAD_EXTRACT_TIMESTAMP in CMake 3.24:
if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.24.0")
  cmake_policy(SET CMP0135 NEW)
endif()

FetchContent_Declare(
  famfam
  URL https://github.com/legacy-icons/famfamfam-silk/archive/refs/tags/1.0.0.tar.gz
  URL_HASH MD5=23972b28cfa3aef966d24337287a2e09)
FetchContent_Declare(
  qslog
  GIT_REPOSITORY https://gitlab.com/syntgroup/libs/QsLog.git
  GIT_TAG e45f5bc0449aeb573abe49712de09b16e9f643e2)
FetchContent_MakeAvailable(famfam qslog)

file(COPY ${famfam_SOURCE_DIR}/src/
     DESTINATION ${CMAKE_SOURCE_DIR}/resources/icons/silk/icons)

find_package(QuaZip-Qt5 REQUIRED)
#find_package(qcustomplot-qt5)
