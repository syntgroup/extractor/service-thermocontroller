install(
    TARGETS service-termocontrollers_exe
    RUNTIME COMPONENT service-termocontrollers_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
