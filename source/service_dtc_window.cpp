#include "service_dtc_window.h"
#include "ui_service_dtc_window.h"

#include "device/thermocontroller.h"
#include "models/connection_params.h"
#include "transfer/executor.h"

ServiceDtcWindow::ServiceDtcWindow(QWidget* parent)
  : QMainWindow{ parent }
  , m_ui{ std::make_unique<Ui::ServiceDtcWindow>() }
  , m_thermocontrollers{ std::make_shared<device::Thermocontrollers>() }
  , m_executor{
    std::make_shared<transfer::Executor>(nullptr, m_thermocontrollers.get())
  }
{
  m_ui->setupUi(this);
}

ServiceDtcWindow::~ServiceDtcWindow() = default;
