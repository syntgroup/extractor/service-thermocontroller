#ifndef DIALOGS_CONNECTION_AUTO_SEARCH_H
#define DIALOGS_CONNECTION_AUTO_SEARCH_H

#include <QDialog>

namespace dialogs {

namespace Ui {
class ConnectionAutoSearch;
}

class ConnectionAutoSearch : public QDialog
{
  Q_OBJECT

public:
  explicit ConnectionAutoSearch(QWidget* parent = nullptr);
  ~ConnectionAutoSearch();

private:
  std::unique_ptr<Ui::ConnectionAutoSearch> ui{ nullptr };
};


} // namespace dialogs
#endif // DIALOGS_CONNECTION_AUTO_SEARCH_H
