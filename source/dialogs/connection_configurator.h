#ifndef DIALOGS_CONNECTION_CONFIGURATOR_H
#define DIALOGS_CONNECTION_CONFIGURATOR_H

#include <QDialog>

namespace dialogs {

namespace Ui {
class ConnectionConfigurator;
}

class ConnectionConfigurator : public QDialog
{
  Q_OBJECT

    public:
      explicit ConnectionConfigurator(QWidget* parent = nullptr);
      ~ConnectionConfigurator() override;

    private:
      std::unique_ptr<Ui::ConnectionConfigurator> ui{ nullptr };
};

} // namespace dialogs
#endif // DIALOGS_CONNECTION_CONFIGURATOR_H
