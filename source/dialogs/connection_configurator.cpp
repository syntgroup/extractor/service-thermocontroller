#include "connection_configurator.h"
#include "ui_connection_configurator.h"

#include <QSerialPort>
#include <QSerialPortInfo>

namespace dialogs {

ConnectionConfigurator::ConnectionConfigurator(QWidget* parent)
  : QDialog{ parent }
  , ui{ std::make_unique<Ui::ConnectionConfigurator>() }
{
  ui->setupUi(this);

  const auto baudRates =
    std::map<int, QSerialPort::BaudRate>{ { 9600, QSerialPort::Baud9600 },
                                          { 38400, QSerialPort::Baud38400 } };
  const auto stopBits =
    std::map<int, QSerialPort::StopBits>{ { 1, QSerialPort::OneStop },
                                          { 2, QSerialPort::TwoStop } };
  const auto dataBits =
    std::map<int, QSerialPort::DataBits>{ { 7, QSerialPort::Data7 },
                                          { 8, QSerialPort::Data8 } };

  for (const auto& baudRate : baudRates) {
    const auto title{ QString::number(baudRate.first) };
    ui->baudRateComboBox->addItem(title, baudRate.second);
    ui->baudRateComboBox_2->addItem(title, baudRate.second);
  }

  for (const auto& stopBit : stopBits) {
    const auto title{ QStringLiteral("%1 stop").arg(stopBit.first) };
    ui->stopBitComboBox->addItem(title, stopBit.second);
    ui->stopBitComboBox_2->addItem(title, stopBit.second);
  }

  for (const auto& dataBit : dataBits) {
    const auto title{ QStringLiteral("%1 data").arg(dataBit.first) };
    ui->bitLengthComboBox->addItem(title, dataBit.second);
    ui->bitLengthComboBox_2->addItem(title, dataBit.second);
  }
}

ConnectionConfigurator::~ConnectionConfigurator() = default;

} // namespace dialogs
