#include "connection_auto_search.h"
#include "ui_connection_auto_search.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QVariant>
#include <device/dtc_config.h>

namespace dialogs {

ConnectionAutoSearch::ConnectionAutoSearch(QWidget* parent)
  : QDialog{ parent }
  , ui{ std::make_unique<Ui::ConnectionAutoSearch>() }
{
  ui->setupUi(this);

  const auto baudRates{ std::vector<QSerialPort::BaudRate>{
    QSerialPort::UnknownBaud,
    QSerialPort::Baud9600,
    QSerialPort::Baud38400,
    QSerialPort::Baud115200 } };
  std::for_each(baudRates.cbegin(), baudRates.cend(), [this](const auto& item) {
    ui->baudRateComboBox->addItem(
      item == -1 ? QVariant::fromValue(item).toString() : QString::number(item),
      item);
  });

  const auto dataBits{ std::vector<QSerialPort::DataBits>{
    QSerialPort::UnknownDataBits, QSerialPort::Data7, QSerialPort::Data8 } };
  std::for_each(dataBits.cbegin(), dataBits.cend(), [this](const auto& item) {
    ui->dataBitsComboBox->addItem(
      item == -1 ? QVariant::fromValue(item).toString() : QString::number(item),
      item);
  });

  const auto stopBits{
    std::vector<QSerialPort::StopBits>{
      QSerialPort::UnknownStopBits, QSerialPort::OneStop, QSerialPort::TwoStop }
  };
  std::for_each(stopBits.cbegin(), stopBits.cend(), [this](const auto& item) {
    ui->stopBitsComboBox->addItem(QVariant::fromValue(item).toString(), item);
  });

  const auto parities{ std::vector<QSerialPort::Parity>{
    QSerialPort::UnknownParity,
    QSerialPort::NoParity,
    QSerialPort::EvenParity,
    QSerialPort::OddParity } };
  std::for_each(parities.cbegin(), parities.cend(), [this](const auto& item) {
    ui->parityComboBox->addItem(QVariant::fromValue(item).toString(), item);
  });

  const auto protocols{ std::vector<device::DtcConfig::COMM_MODE>{
    device::DtcConfig::COMM_MODE::Unknown,
    device::DtcConfig::COMM_MODE::ASCII,
    device::DtcConfig::COMM_MODE::RTU } };
  std::for_each(protocols.cbegin(), protocols.cend(), [this](const auto& item) {
    ui->protocolComboBox->addItem(QVariant::fromValue(item).toString(),
                                  QVariant::fromValue(item));
  });
}

ConnectionAutoSearch::~ConnectionAutoSearch() = default;

} // namespace dialogs
