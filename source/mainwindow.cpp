#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "models/connection_params.h"
#include <QSerialPort>
#include <device/extractor.h>
#include <device/heater.h>

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow{ parent }
  , ui{ std::make_unique<Ui::MainWindow>() }
  , port{ std::make_unique<QSerialPort>() }
  , params{ std::make_unique<models::Settings>() }
//  , extractor{ std::make_unique<device::Extractor>(new QSerialPort,
//                                                   port.get()) }
{
  ui->setupUi(this);

  ui->connection->setSerialPort(port.get(), params.get());
  ui->ra_commands->setSerialPort(port.get(), params.get());

  ui->monitor->setData(port.get());

  auto lambda{ [this](bool status) {
    ui->monitor->setEnabled(status);
    ui->recorder->setEnabled(status);
    ui->ra_commands->setEnabled(status);
  } };
  connect(
    ui->connection, &screens::ConnectionControl::connectPort, this, lambda);
}

MainWindow::~MainWindow() = default;
