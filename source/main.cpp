#include "main.h"

#include "service_dtc_window.h"
#include <QApplication>
#include <QLibraryInfo>
#include <QStandardPaths>
#include <QStyleFactory>
#include <QsLog/QsLog.h>

int
main(int argc, char* argv[])
{
  QApplication application(argc, argv);

  QApplication::setOrganizationName(Main::CompanyName());
  QApplication::setOrganizationDomain(Main::CompanyDomain());
  QApplication::setApplicationName(QLatin1String("SERVICE DTC100"));
  QApplication::setApplicationDisplayName(QApplication::applicationName());
  //  QApplication::setStyle(QStyleFactory::create(QStringLiteral("Fusion")));

  const auto sLogPath{ QStandardPaths::writableLocation(
                         QStandardPaths::GenericDataLocation) +
                       QStringLiteral("/") + QApplication::organizationName() +
                       QStringLiteral("/") + QApplication::applicationName() +
                       QStringLiteral(".log") };

  QsLogging::Logger& logger{ QsLogging::Logger::instance() };

  logger.setLoggingLevel(QsLogging::TraceLevel);
  auto fileDestination(QsLogging::DestinationFactory::MakeFileDestination(
    sLogPath,
    QsLogging::EnableLogRotation,
    QsLogging::MaxSizeBytes(10e7),
    QsLogging::MaxOldLogCount(10)));
  logger.addDestination(fileDestination);
  auto debugDestination(
    QsLogging::DestinationFactory::MakeDebugOutputDestination());
  logger.addDestination(debugDestination);

  QLOG_INFO().noquote() << "============"
                        << "APP VERSION" << QApplication::applicationVersion()
                        << "STARTED"
                        << "============";
  QLOG_INFO().noquote() << "MAIN"
                        << "built with qt" << QLatin1String(QT_VERSION_STR)
                        << "running on" << QSysInfo::kernelType()
                        << QSysInfo::kernelVersion() << QLibraryInfo::version();
  QLOG_INFO() << "MAIN"
              << "built from" << BuildInfo::GitBranch() << "branch"
              << "commit" << BuildInfo::GitCommitHash() << "on"
              << BuildInfo::GitCommitTimestamp().toString(
                   QStringLiteral("yyyy-MM-dd/HH:mm"));
#ifdef QT_DEBUG
  QLOG_INFO() << "MAIN"
              << "build configuration"
              << "DEBUG";
#else
  QLOG_INFO() << "MAIN"
              << "build configuration"
              << "RELEASE";
#endif

  auto window{ std::make_unique<ServiceDtcWindow>() };
  window->show();

  auto ret_status{ QApplication::exec() };

  QLOG_INFO() << "============"
              << "APP FINISHED WITH CODE" << ret_status << "============";
  QsLogging::Logger::destroyInstance();

  return ret_status;
}
