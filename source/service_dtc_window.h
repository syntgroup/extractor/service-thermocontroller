#ifndef SERVICE_APP_DTC_MAINWINDOW_H
#define SERVICE_APP_DTC_MAINWINDOW_H

#include <QMainWindow>

namespace device {
class Thermocontrollers;
} // namespace device

namespace transfer {
class Executor;
} // namespace transfer

namespace Ui {
class ServiceDtcWindow;
} // namespace Ui

class ServiceDtcWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit ServiceDtcWindow(QWidget* parent = nullptr);
  ~ServiceDtcWindow() override;

private:
  std::unique_ptr<Ui::ServiceDtcWindow> m_ui{ nullptr };
  std::shared_ptr<device::Thermocontrollers> m_thermocontrollers{ nullptr };
  std::shared_ptr<transfer::Executor> m_executor{ nullptr };
};

#endif // SERVICE_APP_DTC_MAINWINDOW_H
