#ifndef MODELS_CONNECTIONPARAMS_H
#define MODELS_CONNECTIONPARAMS_H

#include <QSerialPort>
#include <device/dtc_config.h>

namespace models {

struct Settings
{
  uint8_t adr{ 0 };
  QString name{};
  QSerialPort::BaudRate baudRate{ QSerialPort::Baud9600 };
  QSerialPort::DataBits dataBits{ QSerialPort::Data7 };
  QSerialPort::Parity parity{ QSerialPort::NoParity };
  QSerialPort::StopBits stopBits{ QSerialPort::OneStop };
  QSerialPort::FlowControl flowControl{ QSerialPort::NoFlowControl };
  bool localEchoEnabled{ false };
  device::DtcConfig::COMM_MODE protocol{ device::DtcConfig::COMM_MODE::ASCII };
};

} // namespace models

#endif // MODELS_CONNECTIONPARAMS_H
