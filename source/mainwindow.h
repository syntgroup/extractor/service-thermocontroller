#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QSerialPort;

namespace models {
class Settings;
}

namespace device {
class Extractor;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow() override;

private:
  std::unique_ptr<Ui::MainWindow> ui{ nullptr };
  std::unique_ptr<QSerialPort> port{ nullptr };
  std::unique_ptr<models::Settings> params{ nullptr };
  //  std::unique_ptr<device::Extractor> extractor{ nullptr };
};

#endif // MAINWINDOW_H
