#ifndef SERIALACCESSIBLE_H
#define SERIALACCESSIBLE_H

#include <QString>
#include <memory>

namespace transfer {
class Executor;
} // namespace transfer

template<class T>
class SerialAccessible
{
public:
  SerialAccessible()  = default;
  virtual ~SerialAccessible() = default;

  SerialAccessible(const SerialAccessible&)                        = delete;
  auto operator=(const SerialAccessible&) -> SerialAccessible&     = delete;
  SerialAccessible(SerialAccessible&&) noexcept                    = default;
  auto operator=(SerialAccessible&&) noexcept -> SerialAccessible& = default;

  void setupDevice(std::shared_ptr<T> device,
                   std::shared_ptr<transfer::Executor> executor)
  {
    m_device   = device;
    m_executor = executor;

    setupConnections();
  }

protected:
  [[nodiscard]] auto executor() const { return m_executor; }
  [[nodiscard]] auto device() { return m_device; }

  virtual void setupConnections() = 0;

private:
  std::shared_ptr<T> m_device{ nullptr };
  std::shared_ptr<transfer::Executor> m_executor{ nullptr };
};

#endif // SERIALACCESSIBLE_H
