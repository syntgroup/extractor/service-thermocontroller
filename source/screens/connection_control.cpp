#include "connection_control.h"
#include "ui_connection_control.h"

#include "dialogs/connection_auto_search.h"
#include "dialogs/connection_configurator.h"
#include "models/connection_params.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <device/dtc_config.h>

namespace screens {

ConnectionControl::ConnectionControl(QWidget* parent)
  : QWidget{ parent }
  , ui{ std::make_unique<Ui::ConnectionControl>() }
{
  ui->setupUi(this);

  connect(ui->b_autoSearch,
          &QPushButton::clicked,
          this,
          &screens::ConnectionControl::loadAutoSearch);
  connect(ui->b_setConnectionParams,
          &QPushButton::clicked,
          this,
          &screens::ConnectionControl::loadConnectionConfigurator);

  connect(ui->cb_serialPortInfo,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,
          &screens::ConnectionControl::showPortInfo);

  connect(ui->b_connect,
          &QPushButton::clicked,
          this,
          &screens::ConnectionControl::portConnect);
  connect(ui->b_disconnect,
          &QPushButton::clicked,
          this,
          &screens::ConnectionControl::portDisconnect);

  connect(ui->b_refreshPorts,
          &QPushButton::clicked,
          this,
          &screens::ConnectionControl::fillPortsInfo);

  //  connect(this,
  //          &screens::ConnectionControl::connectPort,
  //          this,
  //          &screens::ConnectionControl::onPortConnected);
  //  connect(this,
  //          &screens::ConnectionControl::disconnectPort,
  //          this,
  //          &screens::ConnectionControl::onPortDisconnected);

  fillPortsParameters();
  fillPortsInfo();
}

void
ConnectionControl::setSerialPort(QSerialPort* _port,
                                 models::Settings* _settings)
{
  if (port != nullptr) {
    delete port;
    port = nullptr;
  }

  if (settings != nullptr) {
    delete settings;
    settings = nullptr;
  }

  port     = _port;
  settings = _settings;
}

void
ConnectionControl::updateSettings()
{
  settings->name     = ui->cb_serialPortInfo->currentText();
  settings->baudRate = static_cast<QSerialPort::BaudRate>(
    ui->baudRateComboBox->currentData().toUInt());
  settings->dataBits = static_cast<QSerialPort::DataBits>(
    ui->bitLengthComboBox->currentData().toUInt());
  settings->parity = static_cast<QSerialPort::Parity>(
    ui->parityComboBox->currentData().toUInt());
  settings->stopBits = static_cast<QSerialPort::StopBits>(
    ui->stopBitComboBox->currentData().toUInt());
  settings->flowControl = static_cast<QSerialPort::FlowControl>(
    ui->flowControlComboBox->currentData().toUInt());
  settings->protocol = static_cast<device::DtcConfig::COMM_MODE>(
    ui->protocolComboBox->currentData().toUInt());
}

void
ConnectionControl::showPortInfo(int idx)
{
  if (idx < 0) {
    return;
  }

  const auto list{ ui->cb_serialPortInfo->itemData(idx).toStringList() };

  ui->descriptionLabel->setText(QStringLiteral("Description: %1")
                                  .arg(list.count() > portDescription
                                         ? list.at(portDescription)
                                         : QLatin1String()));
  ui->manufacturerLabel->setText(QStringLiteral("Manufacturer: %1")
                                   .arg(list.count() > portManufacturer
                                          ? list.at(portManufacturer)
                                          : QLatin1String()));
  ui->serialNumberLabel->setText(QStringLiteral("Serial number: %1")
                                   .arg(list.count() > portSerialNumber
                                          ? list.at(portSerialNumber)
                                          : QLatin1String()));
  ui->locationLabel->setText(QStringLiteral("Location: %1")
                               .arg(list.count() > portLocation
                                      ? list.at(portLocation)
                                      : QLatin1String()));
  ui->vidLabel->setText(QStringLiteral("Vendor Identifier: %1")
                          .arg(list.count() > portVendorId
                                 ? list.at(portVendorId)
                                 : QLatin1String()));
  ui->pidLabel->setText(QStringLiteral("Product Identifier: %1")
                          .arg(list.count() > portProductId
                                 ? list.at(portProductId)
                                 : QLatin1String()));
}

void
ConnectionControl::apply()
{
}

ConnectionControl::~ConnectionControl() = default;

void
ConnectionControl::loadAutoSearch()
{
  auto dialog{ std::make_unique<dialogs::ConnectionAutoSearch>() };
  dialog->exec();
}

void
ConnectionControl::loadConnectionConfigurator()
{
  auto dialog{ std::make_unique<dialogs::ConnectionConfigurator>() };
  auto ret{ dialog->exec() };
}

void
ConnectionControl::fillPortsParameters()
{
  const auto baudRates{ std::vector<QSerialPort::BaudRate>{
    QSerialPort::Baud9600, QSerialPort::Baud38400, QSerialPort::Baud115200 } };
  std::for_each(baudRates.cbegin(), baudRates.cend(), [this](const auto& item) {
    ui->baudRateComboBox->addItem(QString::number(item), item);
  });
  ui->baudRateComboBox->setCurrentIndex(
    ui->baudRateComboBox->findData(QSerialPort::Baud38400));

  const auto dataBits{ std::vector<QSerialPort::DataBits>{
    QSerialPort::Data7, QSerialPort::Data8 } };
  std::for_each(dataBits.cbegin(), dataBits.cend(), [this](const auto& item) {
    ui->bitLengthComboBox->addItem(QString::number(item), item);
  });
  ui->bitLengthComboBox->setCurrentIndex(
    ui->bitLengthComboBox->findData(QSerialPort::Data8));

  const auto stopBits{ std::vector<QSerialPort::StopBits>{
    QSerialPort::OneStop, QSerialPort::TwoStop } };
  std::for_each(stopBits.cbegin(), stopBits.cend(), [this](const auto& item) {
    ui->stopBitComboBox->addItem(QString::number(item), item);
  });

  const auto parities{ std::vector<QSerialPort::Parity>{
    QSerialPort::NoParity, QSerialPort::EvenParity, QSerialPort::OddParity } };
  std::for_each(parities.cbegin(), parities.cend(), [this](const auto& item) {
    ui->parityComboBox->addItem(QVariant::fromValue(item).toString(), item);
  });
  ui->parityComboBox->setCurrentIndex(
    ui->parityComboBox->findData(QSerialPort::NoParity));

  const auto flowControls{ std::vector<QSerialPort::FlowControl>{
    QSerialPort::NoFlowControl,
    QSerialPort::HardwareControl,
    QSerialPort::SoftwareControl } };
  std::for_each(
    flowControls.cbegin(), flowControls.cend(), [this](const auto& item) {
      ui->flowControlComboBox->addItem(QVariant::fromValue(item).toString(),
                                       item);
    });
  ui->flowControlComboBox->setCurrentIndex(
    ui->flowControlComboBox->findData(QSerialPort::NoFlowControl));

  const auto protocols{ std::vector<device::DtcConfig::COMM_MODE>{
    device::DtcConfig::COMM_MODE::ASCII, device::DtcConfig::COMM_MODE::RTU } };
  std::for_each(protocols.cbegin(), protocols.cend(), [this](const auto& item) {
    ui->protocolComboBox->addItem(QVariant::fromValue(item).toString(),
                                  QVariant::fromValue(item));
  });
}

void
ConnectionControl::fillPortsInfo()
{
  ui->cb_serialPortInfo->clear();

  const auto infos{ QSerialPortInfo::availablePorts() };
  for (const auto& info : infos) {
    const auto list{ QStringList{
      info.portName(),
      !info.description().isEmpty() ? info.description() : QLatin1String(),
      !info.manufacturer().isEmpty() ? info.manufacturer() : QLatin1String(),
      !info.serialNumber().isEmpty() ? info.serialNumber() : QLatin1String(),
      info.systemLocation(),
      info.vendorIdentifier() != 0
        ? QString::number(info.vendorIdentifier(), 16)
        : QLatin1String(),
      info.productIdentifier() != 0
        ? QString::number(info.productIdentifier(), 16)
        : QLatin1String() } };
    ui->cb_serialPortInfo->addItem(list.first(), list);
  }
  ui->cb_serialPortInfo->addItem(QStringLiteral("Custom"));
}

void
ConnectionControl::portConnect()
{
  updateSettings();
  if (port->isOpen()) {
    port->flush();
    port->close();
  }

  port->setPortName(settings->name);
  port->setBaudRate(settings->baudRate);
  port->setDataBits(settings->dataBits);
  port->setStopBits(settings->stopBits);
  port->setParity(settings->parity);
  port->setFlowControl(settings->flowControl);

  if (!port->open(QIODevice::ReadWrite)) {
    emit cantOpenSerialPort();
    return;
  }

  onPortConnected();
  emit connectPort(true);
}

void
ConnectionControl::portDisconnect()
{
  if (!port->isOpen()) {
    return;
  }

  port->close();
  onPortDisconnected();
  emit connectPort(false);
}

void
ConnectionControl::onPortConnected()
{
  ui->b_connect->setEnabled(false);
  ui->b_disconnect->setEnabled(true);
  ui->selectBox->setEnabled(false);
  ui->parametersBox->setEnabled(false);
}

void
ConnectionControl::onPortDisconnected()
{
  ui->b_connect->setEnabled(true);
  ui->b_disconnect->setEnabled(false);
  ui->selectBox->setEnabled(true);
  ui->parametersBox->setEnabled(true);
}

} // namespace screens
