#ifndef SCREENS_RAW_COMMAND_EXECUTOR_H
#define SCREENS_RAW_COMMAND_EXECUTOR_H

#include "dispatchers/contains_device.h"
#include <QWidget>

class QSerialPort;

namespace device {
class Thermocontrollers;
} // namespace device

// namespace protocol {
// namespace modbus {
// class SerialRtuAdu;
// } // namespace modbus
// } // namespace protocol

// namespace models {
// struct Settings;
// } // namespace models

namespace screens {

namespace Ui {
class RawCommandExecutor;
} // namespace Ui

class RawCommandExecutor
  : public QWidget
  , public SerialAccessible<device::Thermocontrollers>
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(RawCommandExecutor)
public:
  explicit RawCommandExecutor(QWidget* parent = nullptr);
  ~RawCommandExecutor() override;

  //  void setHeaters(device::Heaters* newHeaters);
  //  void setSerialPort(QSerialPort* _port, models::Settings* _settings);

  // private slots:
  //   void sendAndRecieveRawCommand();
  //   void sendAndRecievePreDefined(const QByteArray& data);

  //  void onPortReadyRead();

private:
  std::unique_ptr<Ui::RawCommandExecutor> m_ui{ nullptr };
  //  std::unique_ptr<protocol::modbus::SerialRtuAdu> request{ nullptr };
  //  device::Heaters* heaters{ nullptr };
  //  QSerialPort* port{ nullptr };
  //  models::Settings* settings{ nullptr };

signals:
  void timeout(const QString&);

  // SerialAccessible interface
protected:
  void setupConnections() override;
};

} // namespace screens
#endif // SCREENS_RAW_COMMAND_EXECUTOR_H
