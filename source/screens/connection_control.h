#ifndef SCREENS_CONNECTION_CONTROL_H
#define SCREENS_CONNECTION_CONTROL_H

#include <QWidget>

class QSerialPort;

namespace models {
struct Settings;
}

namespace screens {

namespace Ui {
class ConnectionControl;
}

class ConnectionControl : public QWidget
{
  Q_OBJECT

public:
  explicit ConnectionControl(QWidget* parent = nullptr);
  ~ConnectionControl() override;

  void setSerialPort(QSerialPort* _port, models::Settings* _settings);

private:
  std::unique_ptr<Ui::ConnectionControl> ui{ nullptr };
  QSerialPort* port{ nullptr };
  models::Settings* settings{ nullptr };

  enum pos
  {
    portName = 0,
    portDescription,
    portManufacturer,
    portSerialNumber,
    portLocation,
    portVendorId,
    portProductId
  };

  void updateSettings();

private slots:
  void showPortInfo(int idx);
  void apply();

  void loadAutoSearch();
  void loadConnectionConfigurator();

  void fillPortsParameters();
  void fillPortsInfo();

  void portConnect();
  void portDisconnect();

signals:
  void cantOpenSerialPort();
  void connectPort(bool status);
  //  void disconnectPort();

public slots:
  void onPortConnected();
  void onPortDisconnected();
};

} // namespace screens
#endif // SCREENS_CONNECTION_CONTROL_H
