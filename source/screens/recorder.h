#ifndef SCREENS_RECORDER_H
#define SCREENS_RECORDER_H

#include <QWidget>

namespace screens {

namespace Ui {
class Recorder;
}

class Recorder : public QWidget
{
  Q_OBJECT

public:
  explicit Recorder(QWidget* parent = nullptr);
  ~Recorder();

private:
  Ui::Recorder *ui;
};


} // namespace screens
#endif // SCREENS_RECORDER_H
