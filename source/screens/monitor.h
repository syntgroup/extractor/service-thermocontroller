#ifndef SCREENS_MONITOR_H
#define SCREENS_MONITOR_H

#include "dispatchers/contains_device.h"
#include <QWidget>

class QTimer;
class QElapsedTimer;
class QSerialPort;

namespace device {
class Thermocontrollers;
} // namespace device

namespace screens {

namespace Ui {
class Monitor;
} // namespace Ui

class Monitor
  : public QWidget
  , public SerialAccessible<device::Thermocontrollers>
{
  Q_OBJECT

  enum class CurCommand
  {
    Idle,
    requestSV,
    requestPV
  };
  CurCommand current_cmd{ CurCommand::Idle };

public:
  explicit Monitor(QWidget* parent = nullptr);
  ~Monitor() override;

  // public slots:
  //   void setData(QSerialPort* _port);

private:
  std::unique_ptr<Ui::Monitor> ui{ nullptr };
  std::unique_ptr<QTimer> pollTimer{ nullptr };
  std::unique_ptr<QElapsedTimer> elapsedTimer{ nullptr };

  //  device::Heaters* heaters{ nullptr };
  //  QSerialPort* port{ nullptr };

  uint8_t curAdr{ 0 };

  void setupPlotArea();

  double m_pv{ 0 };
  double m_sv{ 0 };
  double m_lastPointKey{ 0 };

private slots:
  void pollPV();
  void onDeviceFound();
  //  void onSerialPortReadyRead();
  void openConnection();
  void closeConnection();

  void onCurrentTemperatureChanged();
  void onTargetTemperatureChanged();

signals:
  void deviceFound();

  // SerialAccessible interface
protected:
  void setupConnections() override;
};

} // namespace screens
#endif // SCREENS_MONITOR_H
