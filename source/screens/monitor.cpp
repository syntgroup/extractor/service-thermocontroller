#include "monitor.h"
#include "ui_monitor.h"

#include "device/heater.h"
#include "device/thermocontroller.h"
#include "transfer/executor.h"
#include <QDebug>
#include <QElapsedTimer>
#include <QSerialPort>
#include <QTimer>
#include <chrono>

namespace {
const auto timeout{ std::chrono::milliseconds{ 500 } };
} // namespace

namespace screens {

Monitor::Monitor(QWidget* parent)
  : QWidget{ parent }
  , ui{ std::make_unique<Ui::Monitor>() }
  , pollTimer{ std::make_unique<QTimer>() }
  , elapsedTimer{ std::make_unique<QElapsedTimer>() }
{
  ui->setupUi(this);

  pollTimer->setSingleShot(true);
  pollTimer->setInterval(timeout);

  connect(ui->b_connectDtc,
          &QPushButton::clicked,
          this,
          &screens::Monitor::openConnection);

  connect(this,
          &screens::Monitor::deviceFound,
          this,
          &screens::Monitor::onDeviceFound);

  connect(ui->sb_addressDtc,
          QOverload<int>::of(&QSpinBox::valueChanged),
          this,
          [this](int value) { curAdr = value; });

  connect(ui->sVDoubleSpinBox,
          QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,
          [this](double value) {
            m_sv = value;
            //            ui->lcdNumber->display(m_sv);
          });

  setupPlotArea();
}

Monitor::~Monitor() = default;

// void
// Monitor::setData(QSerialPort* _port)
//{
//   //  disconnect()
//   if (port != nullptr) {
//     delete port;
//     port = nullptr;
//   }

//  port = _port;
//}

void
Monitor::setupPlotArea()
{
  ui->plot->legend->setVisible(true);
  ui->plot->legend->setFont(QFont("Helvetica", 9));
  ui->plot->legend->setRowSpacing(-3);

  ui->plot->addGraph();
  ui->plot->graph(0)->setName(QStringLiteral("PV"));
  ui->plot->graph(0)->setPen(QPen{ QColor{ 40, 110, 255 } });
  ui->plot->graph(0)->setLineStyle(QCPGraph::lsLine);
  ui->plot->graph(0)->setScatterStyle(QCPScatterStyle::ssSquare);

  ui->plot->addGraph();
  ui->plot->graph(1)->setName(QStringLiteral("SV"));
  ui->plot->graph(1)->setPen(QPen{ QColor{ 255, 110, 40 } });
  ui->plot->graph(1)->setLineStyle(QCPGraph::lsLine);
  ui->plot->graph(1)->setScatterStyle(QCPScatterStyle::ssCircle);

  QSharedPointer<QCPAxisTickerTime> timeTicker{ new QCPAxisTickerTime };
  timeTicker->setTimeFormat("%m:%s");

  ui->plot->xAxis->setTicker(timeTicker);
  ui->plot->axisRect()->setupFullAxesBox();

  ui->plot->yAxis->setRange(QCPRange{ 0, 100 });

  connect(ui->plot->xAxis,
          QOverload<const QCPRange&>::of(&QCPAxis::rangeChanged),
          ui->plot->xAxis2,
          QOverload<const QCPRange&>::of(&QCPAxis::setRange));
  connect(ui->plot->yAxis,
          QOverload<const QCPRange&>::of(&QCPAxis::rangeChanged),
          ui->plot->yAxis2,
          QOverload<const QCPRange&>::of(&QCPAxis::setRange));
}

void
Monitor::pollPV()
{
  qDebug() << "poll routine";

  //  std::unique_ptr<protocol::RequestInterface> request{ nullptr };

  //  if (current_cmd == curCommand::requestPV) {
  //    request =
  //    std::make_unique<protocol::dtc::HeaterCurrentTempRequest>(curAdr);
  //  } else if (current_cmd == curCommand::requestSV) {
  //    request =
  //    std::make_unique<protocol::dtc::HeaterTargetTempRequest>(curAdr);
  //  }

  //  qDebug() << "tx" << request->encode().toHex(':');
  //  port->write(request->encode());
  executor()->getCurrentTemperature(curAdr);
}

void
Monitor::onDeviceFound()
{
  disconnect(ui->b_connectDtc);
  ui->b_connectDtc->setText(QStringLiteral("Disconnect"));
  ui->sb_addressDtc->setEnabled(false);
  ui->cb_connected->setChecked(true);
  ui->pvDoubleSpinBox->setEnabled(true);
  ui->sVDoubleSpinBox->setEnabled(true);
  ui->groupBox_7->setEnabled(true);
  ui->tabWidget->setEnabled(true);
  connect(ui->b_connectDtc,
          &QPushButton::clicked,
          this,
          &screens::Monitor::closeConnection);

  //  connect(port,
  //          &QSerialPort::readyRead,
  //          this,
  //          &screens::Monitor::onSerialPortReadyRead);
  connect(pollTimer.get(), &QTimer::timeout, this, &screens::Monitor::pollPV);

  //  current_cmd = curCommand::requestPV;

  pollTimer->setSingleShot(true);
  pollTimer->start();
  elapsedTimer->start();
}

void
Monitor::openConnection()
{
  //  const auto adr     = static_cast<uint8_t>(ui->sb_addressDtc->value());
  //  const auto request = protocol::dtc::DeviceStateRequest{ curAdr };
  //  qDebug() << "tx" << request.encode().toHex(':');
  //  auto success = false;

  //  port->clear(QSerialPort::AllDirections);
  //  auto bytesWritten = port->write(request.encode());

  //  success = bytesWritten >= 0;
  //  port->waitForBytesWritten(500);

  //  if (port->waitForReadyRead(500)) {
  //    auto responseData{ port->readAll() };
  //    while (port->waitForReadyRead(10)) {
  //      responseData += port->readAll();
  //    }

  //    qDebug() << "recieved" << responseData.toHex(':');
  emit deviceFound();
}

void
Monitor::onCurrentTemperatureChanged()
{
}

void
Monitor::onTargetTemperatureChanged()
{
}

// else
//{
//   qDebug() << "timeout" << QTime::currentTime().toString();
// }
// }

void
Monitor::closeConnection()
{
  disconnect(ui->b_connectDtc);
  //  disconnect(port, nullptr, this, nullptr);

  pollTimer->stop();

  ui->b_connectDtc->setText(QStringLiteral("Connect"));
  ui->sb_addressDtc->setEnabled(true);
  ui->cb_connected->setChecked(false);
  ui->pvDoubleSpinBox->setEnabled(false);
  ui->sVDoubleSpinBox->setEnabled(false);
  ui->groupBox_7->setEnabled(false);
  ui->tabWidget->setEnabled(false);

  connect(ui->b_connectDtc,
          &QPushButton::clicked,
          this,
          &screens::Monitor::openConnection);
}

// void
// Monitor::onSerialPortReadyRead()
//{
//   auto recievedData = port->readAll();
//   qDebug() << "rx" << recievedData.toHex(':');

//  //  const protocol::dtc::HeaterGetCurTempResponse response{ recievedData };
//  //  if (response.isError()) {
//  //    return;
//  //  }

//  //  const auto value = response.cur_temp();
//  double value{ 0 };

//  if (current_cmd == curCommand::requestPV) {
//    current_cmd = curCommand::requestSV;
//    const protocol::dtc::HeaterGetCurTempResponse response{ recievedData };
//    value = response.cur_temp();
//    qDebug() << "get PV" << value;
//  } else if (current_cmd == curCommand::requestSV) {
//    current_cmd = curCommand::requestPV;
//    const protocol::dtc::HeaterGetTargetTempResponse response{ recievedData };
//    value = response.target_temp();
//    qDebug() << "get SV" << value;
//  }

//  //  qDebug() << "recieved pv" << value;
//  current_cmd == curCommand::requestSV ? ui->lcd_pv->display(value)
//                                       : ui->lcd_sv->display(value);

//  //  static QTime time{ QTime::currentTime() };
//  auto key{ elapsedTimer->elapsed() / 1000.0 };

//  lastPointKey = 0;
//  if (key - lastPointKey > 0.002) {
//    // add data to lines:
//    //    ui->plot->graph(0)->addData(key, value);
//    //    ui->plot->graph(1)->addData(key, m_sv);
//    current_cmd == curCommand::requestPV
//      ? ui->plot->graph(1)->addData(key, value)
//      : ui->plot->graph(0)->addData(key, value);
//    // rescale value (vertical) axis to fit the current data:
//    //    ui->plot->graph(0)->rescaleValueAxis();
//    //    ui->plot->graph(1)->rescaleValueAxis(true);
//    lastPointKey = key;
//  }

//  ui->plot->xAxis->setRange(key, 8, Qt::AlignRight);
//  ui->plot->replot();

//  pollTimer->start();
//}

void
screens::Monitor::setupConnections()
{
}

} // namespace screens
