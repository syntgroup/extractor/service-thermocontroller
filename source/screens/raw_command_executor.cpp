#include "raw_command_executor.h"
#include "ui_raw_command_executor.h"

#include "device/heater.h"
#include "device/thermocontroller.h"
#include "models/connection_params.h"
#include "protocols/modbus/modbus_pdu.h"
#include "transfer/executor.h"
#include <QTime>
#include <QsLog/QsLog.h>
//#include <QSerialPort>
//#include <QDebug>
//#include <QFont>

using modbus_pdu_t  = protocol::modbus::ModbusPdu;
using modbus_func_t = protocol::modbus::ModbusPdu::FunctionCode;

namespace screens {

RawCommandExecutor::RawCommandExecutor(QWidget* parent)
  : QWidget{ parent }
  , m_ui{ std::make_unique<Ui::RawCommandExecutor>() }
{
  m_ui->setupUi(this);

  // ---------------------------------------------------------------------------

  for (const auto& command : std::map<modbus_pdu_t::FunctionCode, QString>{
         { modbus_func_t::ReadHoldingRegisters,
           QStringLiteral("Read holding registers") },
         { modbus_func_t::WriteSingleRegister,
           QStringLiteral("Write single register") },
         { modbus_func_t::ReadCoils, QStringLiteral("Read bits") },
         { modbus_func_t::WriteSingleCoil,
           QStringLiteral("Write single bit") } }) {
    const auto title{ QStringLiteral("%1 (0x%2)")
                        .arg(command.second,
                             QString::number(static_cast<int>(command.first),
                                             16)) };
    m_ui->commandComboBox->addItem(title, static_cast<int>(command.first));
  }

  //  auto lambda{ [this](int idx) {
  //    const auto command = ui->commandComboBox->itemData(idx).toUInt();
  //    switch (static_cast<modbus_func_t>(command)) {
  //      case modbus_func_t::ReadHoldingRegisters:
  //        ui->writeDataLabel->setText(QStringLiteral("Word count"));
  //        break;
  //      case modbus_func_t::WriteSingleRegister:
  //        ui->writeDataLabel->setText(QStringLiteral("Write data"));
  //        break;
  //      case modbus_func_t::ReadCoils:
  //        ui->writeDataLabel->setText(QStringLiteral("Bits count"));
  //        break;
  //      case modbus_func_t::WriteSingleCoil:
  //        ui->writeDataLabel->setText(QStringLiteral("Content"));
  //        break;
  //      default:
  //        break;
  //    }
  //  } };
  //  connect(ui->commandComboBox,
  //          QOverload<int>::of(&QComboBox::currentIndexChanged),
  //          this,
  //          lambda);

  // ---------------------------------------------------------------------------
  //  const QFont monospace_font{ QStringLiteral("monospace") };
  //  for (const auto& input : std::vector<QWidget*>{
  //         ui->addressSpinBox,
  //         ui->functionAddressSpinBox,
  //         ui->writeDataLineEdit,
  //         ui->checksumLRCLineEdit,
  //         ui->sendLineEdit,
  //         ui->recievedLineEdit,
  //       }) {
  //    input->setFont(monospace_font);
  //  }

  // ---------------------------------------------------------------------------
  //  connect(ui->b_send,
  //          &QPushButton::clicked,
  //          this,
  //          &screens::RawCommandExecutor::sendAndRecieveRawCommand);

  // ---------------------------------------------------------------------------
  //  auto pid     = 0;
  //  auto encoder = std::make_unique<cmd::dtc::DTCEncoder>();
}

// void
// RawCommandExecutor::sendAndRecieveRawCommand()
//{
//   Q_ASSERT(port);

//  QByteArray result;
//  const auto commandCode{ static_cast<modbus_func_t>(
//    ui->commandComboBox->currentData().toUInt()) };
//  switch (commandCode) {
//    case modbus_func_t::ReadHoldingRegisters: {
//      request = protocol::modbus::SerialRtuAdu::readHoldingRegisters(
//        static_cast<uint8_t>(ui->addressSpinBox->value()),
//        static_cast<uint16_t>(ui->functionAddressSpinBox->value()),
//        static_cast<uint16_t>(ui->writeDataLineEdit->text().toUShort()));
//      result = request->bytes();
//      break;
//    }
//    default: {
//      break;
//    }
//  }

//  ui->sendLineEdit->setText(result.toHex(':'));
//  qDebug() << "sending" << result.toHex(':');

//  port->write(result);
//  port->waitForBytesWritten(500);
//  if (port->waitForReadyRead(500)) {
//    auto responseData{ port->readAll() };
//    while (port->waitForReadyRead(10))
//      responseData += port->readAll();

//    //    const QString response = QString::fromUtf8(responseData);
//    //    emit this->response(response);
//    qDebug() << "recieved" << responseData.toHex(':');
//    ui->recievedLineEdit->setText(responseData.toHex(':'));

//  } else {
//    qDebug() << "timeout" << QTime::currentTime().toString();
//    emit timeout(QStringLiteral("Wait read response timeout %1")
//                   .arg(QTime::currentTime().toString()));
//  }

//  //  qDebug() << Respo
//}

// void
// RawCommandExecutor::sendAndRecievePreDefined(const QByteArray& data)
//{
//   qDebug() << "sending" << data.toHex(':');
//   ui->sendLineEdit->setText(data.toHex(':'));

//  port->write(data);
//  port->waitForBytesWritten(500);
//  if (port->waitForReadyRead(500)) {
//    auto responseData{ port->readAll() };
//    while (port->waitForReadyRead(10))
//      responseData += port->readAll();

//    //    const QString response = QString::fromUtf8(responseData);
//    //    emit this->response(response);
//    qDebug() << "recieved" << responseData.toHex(':');
//    ui->recievedLineEdit->setText(responseData.toHex(':'));

//  } else {
//    qDebug() << "timeout" << QTime::currentTime().toString();
//    emit timeout(QStringLiteral("Wait read response timeout %1")
//                   .arg(QTime::currentTime().toString()));
//  }
//}

RawCommandExecutor::~RawCommandExecutor() = default;

void
RawCommandExecutor::setupConnections()
{
  connect(m_ui->b_getCtrl, &QPushButton::clicked, this, [&] {
    const auto adr = m_ui->addressSpinBox->value();
    //    const auto _request = encoder->deviceCtrlModeRequest(adr);
    //    sendAndRecievePreDefined(_request);
    //    executor().
  });

  connect(m_ui->b_getOut1, &QPushButton::clicked, this, [&] {
    const auto adr = m_ui->addressSpinBox->value();
  });

  connect(m_ui->b_getOut2, &QPushButton::clicked, this, [&] {
    const auto adr = m_ui->addressSpinBox->value();
  });

  connect(m_ui->b_getPV, &QPushButton::clicked, this, [&] {
    const auto address{ m_ui->addressSpinBox->value() };
    executor()->getCurrentTemperature(address);
  });

  connect(m_ui->b_getSV, &QPushButton::clicked, this, [&] {
    const auto address{ m_ui->addressSpinBox->value() };
    executor()->getTargetTemperature(address);
  });

  connect(m_ui->b_setSV20, &QPushButton::clicked, this, [&] {
    const auto address{ m_ui->addressSpinBox->value() };
    const auto target{ 20.0 };
    executor()->setTargetTemperature(address, target);
  });
  connect(m_ui->b_setSV60, &QPushButton::clicked, this, [&] {
    const auto address{ m_ui->addressSpinBox->value() };
    const auto target{ 60.0 };
    executor()->setTargetTemperature(address, target);
  });
  connect(m_ui->b_setSV90, &QPushButton::clicked, this, [&] {
    const auto address{ m_ui->addressSpinBox->value() };
    const auto target{ 90.0 };
    executor()->setTargetTemperature(address, target);
  });
}

} // namespace screens
